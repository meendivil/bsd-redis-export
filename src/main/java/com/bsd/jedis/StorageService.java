package com.bsd.jedis;

import com.bsd.entity.Transaction;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;

/**
 * Created by Edgar.Mendivil on 19/08/2016.
 */
@Component
public class StorageService implements IStorageService {

    public void delete(String key) {
        JedisManagement.GetInstance().deleteValue(key);
    }

    public Set<String> getKeys() {
        return JedisManagement.GetInstance().getKeys();
    }

    public Transaction Fetch (String key) throws IOException {
        Set<String> objectValue = JedisManagement.GetInstance().getValue(key);
        if (objectValue == null) {
            return new Transaction();
        }

        return Transaction.fromJson(objectValue.toArray()[0].toString());
    }
}
