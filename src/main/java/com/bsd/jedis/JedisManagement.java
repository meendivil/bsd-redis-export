package com.bsd.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Set;

/**
 * Created by Edgar.Mendivil on 19/08/2016.
 */
public class JedisManagement {
    private static JedisManagement instance;
    private JedisPool jedisPool;

    protected JedisManagement () {}

    public static JedisManagement GetInstance () {
        if (instance == null) {
            instance = new JedisManagement();
        }
        return instance;
    }

    public Set<String> getValue(String key) {
        Jedis jedisConnection = this.jedisPool.getResource();
        if (!jedisConnection.exists(key)) {
            return null;
        }

        return jedisConnection.smembers(key);
    }

    public Set<String> getKeys(){
        return  this.jedisPool.getResource().keys("STORAGE:*");
    }

    public void deleteValue(String key) {
        Jedis jedisConnection = this.jedisPool.getResource();
        jedisConnection.del(key);
    }

    public void Setup(String host, Integer port) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(1000);
        config.setMaxIdle(10);
        config.setMinIdle(1);
        config.setMaxWaitMillis(30000);
        config.setTestOnBorrow(true);

        this.jedisPool = new JedisPool(config, host, port, 10000);
    }

    public void DestroyEnvironment() {
        this.jedisPool.destroy();
    }
}
