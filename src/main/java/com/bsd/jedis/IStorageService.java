package com.bsd.jedis;

import com.bsd.entity.Transaction;

import java.io.IOException;
import java.util.Set;

/**
 * Created by Edgar.Mendivil on 19/08/2016.
 */
public interface IStorageService {
    void delete (String key);
    Set<String> getKeys();
    Transaction Fetch (String key) throws IOException;
}
