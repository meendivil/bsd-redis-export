package com.bsd.boot;

import com.bsd.model.TransactionDAO;
import com.bsd.entity.Transaction;
import com.bsd.jedis.StorageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Edgar.Mendivil on 23/08/2016.
 */
@Service
public class ScheduledTasks {
    private static final Logger LOG = Logger.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    @Autowired
    private TransactionDAO transactionDAO;

    @Scheduled(cron="0 0 0 * * *")
    public void task() throws IOException {
        LOG.info("La exportación a comenzado a las: " + dateFormat.format(new Date()));
        StorageService storageService = new StorageService();
        Set<String> names = storageService.getKeys();
        Iterator<String> it = names.iterator();
        int registers = names.size();

        while (it.hasNext()) {
            String key = it.next();
            Transaction tr = storageService.Fetch(key);
            tr.setKeyHash(key);
            try {
                transactionDAO.save(tr);
                //storageService.delete(key);
            }catch (Exception e){
                System.out.println(e);
            }
        }
        LOG.info("La exportación de " + registers + " registros termino correctamente a las: " + dateFormat.format(new Date()));

    }
}
