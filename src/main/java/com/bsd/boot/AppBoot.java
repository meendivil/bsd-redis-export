package com.bsd.boot;

import com.bsd.jedis.JedisManagement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * Created by Edgar.Mendivil on 23/08/2016.
 */
@Component
public class AppBoot {

    @Value("${redis.host}")
    private String host;
    @Value("${redis.port}")
    private Integer port;

    @PostConstruct
    public void jedisConstruct () throws IOException { JedisManagement.GetInstance().Setup(host, port); }

    @PreDestroy
    public void jedisDestroyer () {
        JedisManagement.GetInstance().DestroyEnvironment();
    }

}
