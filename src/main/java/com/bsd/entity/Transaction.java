package com.bsd.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Edgar.Mendivil on 19/08/2016.
 */
@Entity
@Table(name="transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @NotNull
    private String keyHash;
    @NotNull
    private Date date;
    private String authorizationCode;
    @NotNull
    private boolean canBeProcessed;
    @NotNull
    private String pinpadSerialNumber;
    @NotNull
    private String message;
    @NotNull
    private String traceNumber;
    @NotNull
    private String traceType;
    private String vendorName;
    private String vendorNumber;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        this.date = (Date) formatter.parse(date);
    }

    public String getKeyHash() {
        return keyHash;
    }

    public void setKeyHash(String keyHash) {
        this.keyHash = keyHash;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public boolean getCanBeProcessed() {
        return canBeProcessed;
    }

    public void setCanBeProcessed(boolean canBeProcessed) {
        this.canBeProcessed = canBeProcessed;
    }

    public String getPinpadSerialNumber() {
        return pinpadSerialNumber;
    }

    public void setPinpadSerialNumber(String pinpadSerialNumber) {
        this.pinpadSerialNumber = pinpadSerialNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public String getTraceType() {
        return traceType;
    }

    public void setTraceType(String traceType) {
        this.traceType = traceType;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorNumber() {
        return vendorNumber;
    }

    public void setVendorNumber(String vendorNumber) {
        this.vendorNumber = vendorNumber;
    }


    public String toJson () throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(this);
    }

    public static Transaction fromJson (String json) throws IOException {
        return new ObjectMapper().readValue(json, Transaction.class);
    }
}
