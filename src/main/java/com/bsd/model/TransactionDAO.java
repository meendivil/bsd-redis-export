package com.bsd.model;

import com.bsd.entity.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Edgar.Mendivil on 19/08/2016.
 */
@Transactional
public interface TransactionDAO extends CrudRepository<Transaction, Long> {
}