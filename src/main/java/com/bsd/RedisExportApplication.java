package com.bsd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class RedisExportApplication{

	public static void main(String[] args){
		SpringApplication.run(RedisExportApplication.class, args);
	}

}
